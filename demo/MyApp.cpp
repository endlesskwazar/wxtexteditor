﻿#include "MyApp.h"

bool MyApp::OnInit()
{
	mainFrame = new MainFrame("TEditor", wxDefaultPosition, wxDefaultSize);
	SetTopWindow(mainFrame);
	mainFrame->Show(true);
	return true;
}

wxIMPLEMENT_APP(MyApp);
